﻿<?php
require_once("db.php");
require_once("recaptchalib.php");

$stopSpam = new stopSpam();
$ip = $_SERVER['REMOTE_ADDR'];

if($stopSpam->dbConnect($stopSpam->host, $stopSpam->user, $stopSpam->password, $stopSpam->database) != true) {
	echo "Fehler";
}
$captcha = recaptcha_get_html($stopSpam->captchaPublic);

if($stopSpam->testIP($ip) == true) {
	die('
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="de-DE">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
    <title>Schutzabfrage</title>
</head>
  <body>
    <h1>Entschuldigung!</h1>
      
    <p>Es wurden ungewöhnliche Aktivitäten aus Ihrem Netzwerk festgestellt. <br />
    Zum Schutz vor automatisierten Abfragen, möchte ich überprüfen, ob Sie ein Mensch
    sind.</p>

    <p>Bitte lösen Sie das folgende CAPTCHA.</p>

    <form method="post" action="verify.php"> ' .

	$captcha . '

      <input type="submit" />
    </form>
  </body>
</html>
');
}