﻿<?php
class stopSpam {
/*
 * Konfiguration
 */
public $host = "localhost";
public $user = "dbuser";
public $password = "dbp4ssw0rd";
public $database = "databasename";
public $file = "/var/webs11/example/path/to/listed_ip_1.txt";
public $destination = "http://url.link";
public $captchaPrivate = "topsecret";
public $captchaPublic = "notsosecret";

/**
 * Verbindung zu Datenbankserver und der Datenbank herstellen
 *
 * @param string host Host
 * @param string user Benutzername
 * @param string password Passwort
 * @param string database Datenbankname
 *
 * @return boolean
 */
public function dbConnect ($host, $user, $password, $database) {
	$link = mysql_connect($host, $user, $password);
	if (!$link) {
		die('Verbindung schlug fehl: ' . mysql_error());
	}

	$db_selected = mysql_select_db($database, $link);
	if (!$db_selected) {
		die ('Kann Datenbank nicht benutzen : ' . mysql_error());
	}

	return true;
}

/**
 * CSV Datei in die Datenbank importieren
 *
 * @param string file absoluter Pfad zur CSV-Datei
 * @param string database Datenbankname in die importiert werden soll
 *
 * @return boolean
 */
public function importCSV ($file, $database) {
	$sql1 = "TRUNCATE `IP`";
	$sql2 = "LOAD DATA LOCAL INFILE '$file'
        INTO TABLE `IP`
        FIELDS TERMINATED BY ';'
        OPTIONALLY ENCLOSED BY '\"'
        LINES TERMINATED BY '\n'
        (`ip`)";

	$result1 = mysql_query($sql1);

	if(!$result1) {
		die('Ungültige Anfrage: ' . mysql_error());
	}
	$result2 = mysql_query($sql2);

	if(!$result2) {
		die('Ungültige Anfrage: ' . mysql_error());
	}

	return true;
}

/**
 * IP auf Vorkommen in Datenbank testen
 *
 * @param string ip IP-Adresse des Besuchers
 *
 * @return boolean
 */
public function testIP($ip) {	
$sql = "SELECT DISTINCT `ip` FROM `IP` WHERE ip = '$ip'";

	$result = mysql_query($sql);
	$result = mysql_fetch_array($result);
	print_r($result);

	if($result != null) {
		return true;
	} else {
		return false;
	}
}

/**
 * Lösung des Captchas überprüfen und IP ggf. löschen
 *
 * @param string ip IP des Besuchers
 */
public function verify($ip) {
    require_once("recaptchalib.php");

    $resp = recaptcha_check_answer ($this->captchaPrivate,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

    if (!$resp->is_valid) {
        // Aktion, wenn das CAPTCHA falsch eingegeben wurde
        die ("Das CAPTCHA wurde falsch eingegeben" .
        "(CAPTCHA said: " . $resp->error . ")");
    }
    else {
       $this->dbConnect($this->host, $this->user, $this->password, $this->database);

       // IP-Adresse löschen
	$sql = "DELETE FROM `IP` WHERE `ip` = '$ip'";

	$result = mysql_query($sql);

	if(!$result) {
		echo mysql_error();
	} else {
		header('Location: ' . $this->destination);
	}
    }
}
}